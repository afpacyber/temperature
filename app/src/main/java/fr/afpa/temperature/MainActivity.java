package fr.afpa.temperature;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText editTextCelsius;
    private EditText editTextFahrenheit;
    private ListView listViewTemperature;
    private List<String> stringList = new ArrayList<>();
    private ArrayAdapter<String> adapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // affichage du Menu (R.menu.menu)
        getMenuInflater().inflate(R.menu.menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menuDelete:
                editTextFahrenheit.setText(null);
                editTextCelsius.setText(null);
                //editTextCelsius.getText().clear();
                adapter.clear();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextCelsius = (EditText) findViewById(R.id.editTextCelsius);
        editTextFahrenheit = (EditText) findViewById(R.id.editTextFahrenheit);
        listViewTemperature = (ListView) findViewById(R.id.listViewTemperature);

        editTextCelsius.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String valeur = s.toString();
                //String valeur2 = editTextCelsius.getText().toString();

                if(editTextCelsius.hasFocus()) {
                    if (!valeur.isEmpty()
                            && TemperatureConverter.isNumeric(valeur)) {
                        String resultat = TemperatureConverter.fahrenheitFromCelcius(Double.valueOf(valeur));

                        editTextFahrenheit.setText(resultat);

                        Log.e("celsius", "valeur: " + valeur);
                    } else {
                        //editTextFahrenheit.setText("");
                        editTextFahrenheit.setText(null);
                    }
                }
            }
        });

        editTextFahrenheit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String valeur = s.toString();

                if(editTextFahrenheit.hasFocus()) {
                    if (!valeur.isEmpty()
                            && TemperatureConverter.isNumeric(valeur)) {
                        String resultat = TemperatureConverter.celsiusFromFahrenheit(Double.valueOf(valeur));

                        editTextCelsius.setText(resultat);
                    } else {
                        editTextCelsius.setText(null);
                    }
                }
            }
        });

        // gestion de la ListView
        adapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_list_item_1,
                stringList);
        listViewTemperature.setAdapter(adapter);
    }

    public void save(View view) {

        if(!editTextCelsius.getText().toString().isEmpty() && !editTextFahrenheit.getText().toString().isEmpty()) {
            //stringList.add(editTextCelsius.getText().toString() + "°C est égal à " + editTextFahrenheit.getText().toString() + "°F");
            stringList.add(
                    String.format(
                            getString(R.string.main_text_convert),
                            editTextCelsius.getText().toString(),
                            editTextFahrenheit.getText().toString()
                    )
            );

            // rafraichissement de la ListView
            adapter.notifyDataSetChanged();
        }
    }


}
